
all: sample

.PHONY: sample
sample:
	$(MAKE) -C sample

check:
	atheist -i3 -veo test/

clean:
	$(RM) *~
