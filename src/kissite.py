#!/usr/bin/python
# -*- mode:python; coding:utf-8 -*-
"""\
usage: {0} <template.tmpl> <source xml files>
       {0} -e <template.xml.tmpl>
"""


import sys
import os
import datetime
import StringIO
import subprocess as subp
import operator
import logging

from lxml import etree
import jinja2

from loggingx import CapitalLoggingFormatter

log = logging.getLogger('kissite')
log.propagate = False
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
console.setFormatter(CapitalLoggingFormatter('[%(levelcapital)s] %(message)s'))
log.addHandler(console)


class FakeFile(StringIO.StringIO):
    def __init__(self, text, name=None):
        self.name = name
        StringIO.StringIO.__init__(self, text)


class StringConstructible:
    @classmethod
    def fromstring(cls, text, fname='<str>'):
        return cls(FakeFile(text, fname))


class Node:
    attrs = []

    def __getattr__(self, name):
        if name not in self.attrs:
            raise AttributeError(name)

        return self.node.attrib.get(name, '')

    def content(self):
        retval = u''
        retval += self.node.text

        for child in self.node.getchildren():
            if child.tag != 'section':
                retval += etree.tostring(child)

        return retval

    def __str__(self):
        return etree.tostring(self.node)


class Section(Node):
    attrs = ['title', 'href']

    def __init__(self, node):
        self.node = node

    def sections(self):
        return [Section(x) for x in self.node.getchildren() if x.tag == 'section']


class Page(Section, StringConstructible):
    attrs = ['name', 'title', 'menu', 'order', 'baseof']

    def __init__(self, fd):
        log.info("loading {0}".format(fd.name))

        fd.seek(0)
        self.fd = fd
        self.href = os.path.splitext(fd.name)[0]
        content = fd.read()

        try:
            self.node = etree.fromstring(content)
        except etree.XMLSyntaxError:
            raise Exception("XML parse error in '{0}'".format(self.href))

    def __eq__(self, other):
        return self.fd == other.fd

    def __repr__(self):
        return '<Page {0}>'.format(self.name)


class Template(StringConstructible):
    def __init__(self, fd):
        env = jinja2.Environment(
            loader=jinja2.DictLoader({'template': fd.read().decode('utf-8')}))

        self.template = env.get_template('template')
        self.context = {'time': datetime.datetime.now()}

    def render_page(self, page):
        retval = self.template.render(page=page, **self.context)
        return retval.strip()

    def render_site(self, site):
        retval = {}
        for page in site.pages:
            content = self.template.render(
                site=site,
                page=page,
                **self.context)
            retval[page.href] = content
        return retval


class Menu(list):
    def __init__(self, name):
        self.name = name
        self.parent = None   # parent menu
        self.base = None     # page base
        list.__init__(self)

    def show(self):
        print "\nMenu '%s'" % self.name
        print 'parent:', self.parent.name if self.parent else ''
        print 'base:', self.base.name if self.base else ''

        for page in self:
            print '-', page.name
        print


class Site:
    def __init__(self, fs, page_names):

        def get_menu(name):
            if name not in self.menus:
                log.info("New menu: '%s'" % name)
                self.menus[name] = Menu(name)
            return self.menus[name]

        def get_breadcrumb(page):
#            print "page.name:", page.name
#            print "page.menu:", page.menu
#            print

            if page.menu == 'root':
                return []

            menu = get_menu(page.menu)
            if menu.base is None:
                log.warning("menu '%s' has no base" % menu.name)
                return []

            return get_breadcrumb(menu.base) + [menu.base]

        self.menus = {'root': Menu('root')}

        self.pages = [Page(fs[x]) for x in page_names]
        for page in self.pages:
            if not page.menu:
                continue

#            print page.name, '-->', page.menu
            get_menu(page.menu).append(page)

            if page.baseof:
#                print page.name, '(base)-->', page.baseof
                get_menu(page.baseof).base = page
                get_menu(page.baseof).parent = self.menus[page.menu]

        # sort menus by @order
        log.info("sorting menus...")
        for menu in self.menus.values():
            menu.sort(key=operator.attrgetter('order'))

        log.info("building breadcrumbs...")
        for page in self.pages:
            page.breadcrumb = get_breadcrumb(page)

        # show menus
#        for key,menu in self.menus.items():
#            menu.show()


class FSLoader(dict):
    def __getitem__(self, name):
        return file(name)


def main(template, *pages):
    tmpl = Template(file(template))
    site = Site(FSLoader(), pages)
    result = tmpl.render_site(site)

    for name, content in result.items():
        with file(name + '.html', 'w') as fd:
            fd.write(content.encode('utf-8'))
            log.info("saving {0}".format(fd.name))


def exec_scripts(fname):
    """run 'exec' in the template and substitute output"""

    def exec_cmd(cmd):
        log.info("'executing '%s'" % cmd)
        return subp.Popen(cmd, shell=True, stdout=subp.PIPE).communicate()[0]

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(['.', '/']))
    template = env.get_template(fname)
    output = template.render(exec_cmd=exec_cmd)
    print output.encode('utf-8').strip()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print __doc__.format(__file__)
        sys.exit(1)

    if sys.argv[1] == '-e':
        exec_scripts(sys.argv[2])
        sys.exit(0)

    main(*sys.argv[1:])
